# Vacuum Pump Controller Leybold TD20

EPICS module to provide communications and read/write data from/to Leybold TD20 vacuum pump controller

## Startup Examples

`iocsh -r vac_ctrl_leyboldtd20,catania -c 'requireSnippet(vac_ctrl_leyboldtd20_ethernet.cmd, "DEVICENAME=LNS-LEBT-010:VAC-VEPT-02100, IPADDR=10.4.0.213, PORT=4002)'`

If ran as proper ioc service:
```
epicsEnvSet(DEVICENAME, "LNS-LEBT-010:VAC-VEPT-02100")
epicsEnvSet(IPADDR, "10.10.1.21")
epicsEnvSet(PORT, "4002")
require vac_ctrl_leyboldtd20, 2.0.0-catania
< ${REQUIRE_vac_ctrl_leyboldtd20_PATH}/startup/vac_ctrl_leyboldtd20_ethernet.cmd
```
