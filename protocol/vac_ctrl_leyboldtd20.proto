################################################################################
# Protocol file for Oerlikon Leybold TD20 Turbo Pump Controller
#
# The protocol is BIG-ENDIAN
#
# Fixed Length Telegram -16 bytes:
# STX - start of text   = 2
# LGE - telegram length = 14
# ADR - address         = 0
# Net Data Block (12 bytes)
# BCC - checksum        = XOR
#
# Where - Net Data Block:
#     PKW area (8 bytes):
#         PKE  - Parameter ID    (2 bytes) = AK + PNU
#         RSV  - Reserved        (1 byte)
#         IND  - Index           (1 byte)
#         PWE  - Parameter value (4 bytes)
#     PZD area - command/reply (4 bytes):
#         STW/ZSW - Control/Status word (2 bytes)
#         HSW/HIW - Main Setpoint/Value (2 bytes)
#
################################################################################

locktimeout  =21000;
# ReplyTimeout should not be more than 25 if you have a direct link to the serial converter
replytimeout = 1900;
readtimeout  =  100;
maxinput     =   16;


lge          =   14;
adr          =    0;
rsv          =    0;
header       = STX $lge $adr;
idx          = 0;
pwe_none     = 0 0 0 0;
stw_none     = 0 0;
zsw_ign      = "\?\?";
hsw_none     = 0 0;
pwe_set_all  = "%.4r";
pwe_get_all  = "%4r";
pwe_ign      = "\?\?\?\?";
hsw_ign      = "\?\?";
bcc          = "%<xor>";

#
# Structure of PKE field
#
# | <- AK ->| 0  |   <- PNU         ->|
# 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 0
# ___________ Type of parameter access
#             __ Reserved
#                ______________________ Parameter number
#

# Type of parameter access
#  Queries
pke_no_access   =    0;		# No access
pke_param_value = 0x10;		# Parameter value requested, but only if parameter number is less than 256
pke_param_value_500 = 0x11;	# Parameter value requested, but only if parameter number is between 256 and 500
pke_wr_16_value = 0x20;		# Write a 16 bit value
pke_wr_32_value = 0x30;		# Write a 32 bit value
pke_field_value = 0x60;		# Field value requested
pke_wr_16_field = 0x70;		# Write a 16 bit field value
pke_wr_32_field = 0x80;		# Write a 32 bit field value
pke_nr_of_elems = 0x90;		# Number of field elements of a field requested
#  Replies
pke_16bit_sent  = 0x10;		# 16bit value is sent
pke_32bit_sent  = 0x20;		# 32bit value is sent
pke_16bit_field = 0x40;		# 16bit field value is sent
pke_32bit_field = 0x50;		# 32bit field value is sent
pke_field_elems = 0x60;		# Number of field elements is sent
pke_cannot_run  = 0x07;		# The frequency converter can not run the command
pke_no_perm     = 0x80;		# During a write access: no permission to write


@replytimeout
{
	disconnect;
	connect 1000;
}


#	    STX  LGE  ADR  PKE  RSVD  IND  PWE  STW/ZSW  HSW  BCC

#########################################################################################
#
# Enable this handler if you want to debug protocol errors
# Do not forget to load the necessary mismatch.db in the startup script
#
#########################################################################################
#@mismatch
#{
#	in  $header "%(iPKE_\${1}_R)2r" $rsv "%(iIDX_\${1}_R)1r" "%(iPWE_\${1}_R)4r"  "%(iZSW_\${1}_R)2r"  "%(iHSW_\${1}_R)2r"  $bcc ;
#}


#########################################################################################
# Set STW
#########################################################################################
put_control
{
	out $header  $pke_no_access  0  $rsv  $idx  $pwe_none  "%.2r"    $hsw_none  $bcc ;
	in  $header  $pke_no_access  0  $rsv  $idx  "%*4r"     $zsw_ign  $hsw_ign   $bcc ;

	@mismatch
	{
		in  $header  "%(\${1}:iCONTROL_PKE)2r"  $rsv  $idx  "%(\${1}:CONTROL_ERROR)4r"  $zsw_ign  $hsw_ign  $bcc ;
	}
}


#########################################################################################
# Read ZSW
#########################################################################################
get_status
{
	out $header  $pke_no_access  0  $rsv  $idx  $pwe_none  $stw_none  $hsw_none  $bcc ;
	in  $header  $pke_no_access  0  $rsv  $idx  "%*4r"     "%2r"      $hsw_ign   $bcc ;
}


#########################################################################################
# Get 16bit parameter value, parameter number must be less than 256
# Protocol arguments:
#  1: Parameter number
#########################################################################################
get_param_val_256
{
	out $header  $pke_param_value  $1  $rsv  $idx  $pwe_none     $stw_none  $hsw_none  $bcc ;
	in  $header  $pke_16bit_sent   $1  $rsv  $idx  $pwe_get_all  $zsw_ign   $hsw_ign   $bcc ;
}


#########################################################################################
# Set 16bit parameter value, parameter number must be less than 256
# Protocol arguments:
#  1: Parameter number
#  2: PREFIX
#########################################################################################
set_param_val_256
{
	out $header  $pke_wr_16_value  $1  $rsv  $idx  $pwe_set_all  $stw_none  $hsw_none  $bcc ;
	in  $header  $pke_16bit_sent   $1  $rsv  $idx  $pwe_get_all  $zsw_ign   $hsw_ign   $bcc ;

	@mismatch
	{
		in  $header  "%(\${2}:WriteResultR.RVAL)r"  $1  $rsv  $idx  "%(\${2}:WriteErrorR)r"  $zsw_ign  $hsw_ign  $bcc ;
	}
}


#########################################################################################
# Get 32bit parameter value, parameter number must be less than 256
# Protocol arguments:
#  1: Parameter number
#########################################################################################
get_param_val32_256
{
	out $header  $pke_param_value  $1  $rsv  $idx  $pwe_none     $stw_none  $hsw_none  $bcc ;
	in  $header  $pke_32bit_sent   $1  $rsv  $idx  $pwe_get_all  $zsw_ign   $hsw_ign   $bcc ;
}


#########################################################################################
# Get 16bit parameter value, parameter number must be between 256-500
# Protocol arguments:
#  1: Parameter number
# NOTE:
#  The protocol argument must be (parameter number - 256), eg. for parameter 303, the protocol argument is 303-256=47
#########################################################################################
get_param_val_500
{
	out $header  $pke_param_value_500  $1  $rsv  $idx  $pwe_none     $stw_none  $hsw_none  $bcc ;
	in  $header  $pke_param_value_500  $1  $rsv  $idx  $pwe_get_all  $zsw_ign   $hsw_ign   $bcc ;
}


#########################################################################################
# Set 16bit parameter value, parameter number must be must be between 256-500
# Protocol arguments:
#  1: Parameter number
# NOTE:
#  The protocol argument must be (parameter number - 256), eg. for parameter 303, the protocol argument is 303-256=47
#########################################################################################
set_param_val_500
{
	out $header  $pke_param_value_500  $1  $rsv  $idx  $pwe_set_all  $stw_none  $hsw_none  $bcc ;
	in  $header  $pke_param_value_500  $1  $rsv  $idx  $pwe_get_all  $zsw_ign   $hsw_ign   $bcc ;
}


#########################################################################################
# Get 16bit field value
# Protocol arguments:
#  1: Parameter number
#  2: Index value of field
#########################################################################################
get_field_val_256
{
	out $header  $pke_field_value  $1  $rsv  $2  $pwe_none     $stw_none  $hsw_none  $bcc ;
	in  $header  $pke_16bit_field  $1  $rsv  $2  $pwe_get_all  $zsw_ign   $hsw_ign   $bcc ;
}


#########################################################################################
# Get 16bit field value
# Protocol arguments:
#  1: Parameter number
#  2: The name of the PV that holds the index value of the requested field
#########################################################################################
get_16bit_field_val_dyn_256
{
	out $header  $pke_field_value  $1  $rsv  "%(\${2})1r"  $pwe_none     $stw_none  $hsw_none  $bcc ;
#FIXME: we should check if the index number in the reply is the same as in the request
	in  $header  $pke_16bit_field  $1  $rsv  "%*1r"        $pwe_get_all  $zsw_ign   $hsw_ign   $bcc ;
}


#########################################################################################
# Get 32bit field value
# Protocol arguments:
#  1: Parameter number
#  2: The name of the PV that holds the index value of the requested field
#########################################################################################
get_32bit_field_val_dyn_256
{
	out $header  $pke_field_value  $1  $rsv  "%(\${2})1r"  $pwe_none     $stw_none  $hsw_none  $bcc ;
#FIXME: we should check if the index number in the reply is the same as in the request
	in  $header  $pke_32bit_field  $1  $rsv  "%*1r"        $pwe_get_all  $zsw_ign   $hsw_ign   $bcc ;
}


#########################################################################################
# Get Profibus address (parameter number 918)
# Protocol arguments:
#  1: Parameter number
# NOTE:
#  While this particular protocol does not use any arguments, the global @mismatch
#  handler needs it
#########################################################################################
get_profibus_addr
{
	out $header  0x13  0x96  $rsv  $idx  $pwe_none     $stw_none  $hsw_none  $bcc ;
	in  $header  0x13  0x96  $rsv  $idx  $pwe_get_all  $zsw_ign   $hsw_ign   $bcc ;
}


#########################################################################################
# Get Failure ID (parameter number 947)
# Protocol arguments:
#  1: Parameter number
# NOTE:
#  While this particular protocol does not use any arguments, the global @mismatch
#  handler needs it
#########################################################################################
get_failure_id
{
	out $header  0x13  0xB3  $rsv  $idx  $pwe_none     $stw_none  $hsw_none  $bcc ;
	in  $header  0x13  0xB3  $rsv  $idx  $pwe_get_all  $zsw_ign   $hsw_ign   $bcc ;
}


#########################################################################################
# Get the number of field elements
# Protocol arguments:
#  1: Parameter number
#########################################################################################
get_fields_num
{
	out $header  $pke_nr_of_elems  $1  $rsv  $idx  $pwe_none     $stw_none  $hsw_none  $bcc ;
	in  $header  $pke_field_elems  $1  $rsv  $idx  $pwe_get_all  $zsw_ign   $hsw_ign   $bcc ;
}


#########################################################################################
# Get Catalogue number/Product Name/Serial Number
# Protocol arguments:
#  1: Parameter number  //see Get Failure ID about why it is mandatory
#  2: Parameter number - 256; eg. for parameter number 312, the argument is 56
#  3: The name of the PV that holds which character to read
#########################################################################################
get_cps_n
{
	out $header  0x61  $2  $rsv  "%(\${3})r"  $pwe_none   $stw_none  $hsw_none  $bcc ;
	in  $header  0x41  $2  $rsv  "%*r"        "\?\?\?%r"  $zsw_ign   $hsw_ign   $bcc ;
}


#########################################################################################
# Saves the settings to EEPROM
# Protocol arguments:
#  1: Parameter number  //see Get Failure ID about why it is mandatory
#########################################################################################
save_settings
{
	out $header  $pke_wr_32_value  8  $rsv  $idx  $pwe_set_all  $stw_none  $hsw_none  $bcc ;
	in  $header  $pke_32bit_sent   8  $rsv  $idx  $pwe_ign      $zsw_ign   $hsw_ign   $bcc ;
}


#########################################################################################
#########################################################################################
#########################################################################################
